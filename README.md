# RssMess

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `rss_mess` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:rss_mess, "~> 0.1.0"}]
    end
    ```

  2. Ensure `rss_mess` is started before your application:

    ```elixir
    def application do
      [applications: [:rss_mess]]
    end
    ```

